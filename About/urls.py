from django.conf.urls import patterns, include, url
from django.conf.urls import *
from django.contrib import admin
from django.contrib.sitemaps import GenericSitemap
from django.views.generic import TemplateView

from About import views
from About.models import Post, Project
from About.sitemap import BlogSitemap


handler500 = views.server_error
handler404 = views.site_error

admin.autodiscover()

post_dict = {
    'queryset': Post.objects.all(),
    'date_field': 'created',
}

project_dict = {
'queryset': Project.objects.all(),
}

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', views.index, name='index'),
                       url(r'^blog/$', views.blog, name='blog'),
                       url(r'^blog/category/([0-9])/$', views.category, name='category'),
                       url(r'^post/(?P<post_id>.+)/$', views.post, name='post'),
                       url(r'^about/$', views.about, name='about'),
                       url(r'^projects/$', views.projects, name='projects'),
                       url(r'^robots\.txt', TemplateView.as_view(template_name="robots.txt")),
                       #url(r'^blog_posts/$', views.blog_posts, name='blog_posts'),
)

sitemaps = {
    'blog': GenericSitemap(post_dict, priority=0.6),
    'pages': BlogSitemap(['blog',
                          # 'blog/category',
                          #'blog/archive', 
                          'about',
                          'projects']),
}

urlpatterns += patterns('',
                        (r'^sitemap\.xml', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
)

