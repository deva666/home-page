from django.contrib import sitemaps
import  datetime

class BlogSitemap(sitemaps.Sitemap):
	def __init__(self, names):
		self.names =names

	def items(self):
		return self.names

	def changefreq(self, obj):
		return 'weekly'

	def lastmod(self, obj):
		return  datetime.datetime.now()

	def location(self, obj):
		return '/'+ obj +'/'


