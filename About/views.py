from time import strptime
from itertools import groupby
from datetime import datetime
import calendar
import math

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from models import Post, Category, Project
from django.core.urlresolvers import reverse


def index(request):
    context = RequestContext(request)
    context_dict = {}

    latest_post = Post.objects.all().order_by('-created')[:1]
    latest_post = init_post(latest_post.get(), request)

    context_dict['latest_post'] = latest_post

    return render_to_response('index.html', context_dict, context)

def blog(request):
    context = RequestContext(request)
    page = 1
    if request.method == 'GET':
        try:
            page = int(request.GET.get('datapage', 1))
        except:
            pass
    return render_to_response('blog.html', get_blog_context_dict(page=page), context)


def category(request, category_id):
    context = RequestContext(request)
    return render_to_response('blog.html', get_blog_context_dict(category_id), context)


def post(request, post_id):
    context = RequestContext(request)
    context_dict = {}
    post_title = post_id.replace('_', ' ')

    try:
        p = Post.objects.get(title=post_title)
    except Post.DoesNotExist:
        return site_error(request)

    p = init_post(p, request)

    context_dict['post'] = p

    return render_to_response('post.html', context_dict, context)


def init_post(pst, request):
    pst.url = request.build_absolute_uri(reverse(post, args=(pst.title.replace(' ', '_'),)))
    try:
        next_p = Post.objects.filter(created__gt=pst.created).order_by('created')[:1]
    except Post.DoesNotExist:
        next_p = None
    try:
        prev = Post.objects.filter(created__lt=pst.created).order_by('-created')[:1]
    except Post.DoesNotExist:
        prev = None
    if next_p:
        pst.next = next_p.get().title.replace(' ', '_')
    if prev:
        pst.prev = prev.get().title.replace(' ', '_')

    return pst


def about(request):
    context = RequestContext(request)
    return render_to_response('about.html', {}, context)


def projects(request):
    context = RequestContext(request)
    projects_all = Project.objects.all()
    return render_to_response('projects.html', {'projects': projects_all}, context)


def site_error(request):
    context = RequestContext(request)
    response = render_to_response('404.html', context_instance=context)
    response.status_code = 404
    return response


def server_error(request):
    context = RequestContext(request)
    response = render_to_response('500.html', context_instance=context)
    response.status_code = 500
    return response


def get_blog_context_dict(cat_id=None, date=None, page=1):
    context_dict = {}
    cat = None
    limit = 5
    top = page * limit
    bottom = (page - 1) * limit

    if cat_id is not None:
        cat = Category.objects.get(id=cat_id)
        posts = Post.objects.filter(categories=cat).order_by('-created')
    elif date is not None:
        date_range = date.split('_')
        year = date_range[1]
        month = list(calendar.month_name).index((date_range[0]))
        posts = Post.objects.filter(created__year=year, created__month=month).order_by('-created')
        date = date.replace('_', ', ')
    else:
        posts = Post.objects.all().order_by('-created')[bottom: top]

    for p in posts:
        p.url = p.title.replace(' ', '_')

    posts_count = Post.objects.all().count()

    context_dict['top'] = top
    context_dict['posts'] = posts
    context_dict['page'] = page
    context_dict['posts_count'] = posts_count
    context_dict['pages'] = 'x' * int(math.ceil(posts_count / top))
    context_dict['category'] = cat
    context_dict['date'] = date

    return context_dict

