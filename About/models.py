from django.db import models

class Project(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField()
    code_url = models.URLField(null=True, blank=True)
    mirror_url = models.URLField(null=True, blank=True)
    download_url = models.URLField(null=True, blank=True)
    views = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)
    platform = models.CharField(max_length=50, null=True, blank=True)
    source_code = models.NullBooleanField()
    
    def __unicode__(self):
        return self.name

    def platform_list(self):
	    if self.platform:
		    return self.platform.split(',')
	    else:
		    return self.platform



class Category(models.Model):
    title = models.CharField(max_length=40, unique=True)
    
    def __unicode__(self):
        return self.title
    
class Post(models.Model):
    title = models.CharField(max_length=128, unique=True)
    text = models.TextField()
    text_clean = models.TextField(null=True, blank=True)
    views = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)
    #code_downloads = models.IntegerField(default=0)
    created = models.DateTimeField()
    categories = models.ManyToManyField(Category)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
	    return '/post/' + self.title.replace(' ','_')
    
    
    
