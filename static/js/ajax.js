﻿var fadeLength = 1000,
       lasturl = '',
       defaultContent = '',
       isLoading = false;

function loadContent(event) {
    event.preventDefault();
    var content;
    var src = event.target.parentNode.getAttribute("href") || event.target.getAttribute("href");
    var url = src;
    if (arguments[1]) {
        $.get(url, { datapage: arguments[1] ? arguments[1] : 'none' }).done(function (data) {
            content = data;
            if ($('#loader').length) {
                replaceContent(content, src);
            }

        }).fail(function () {
            window.location.replace(url);
        });
    } else {
        $.get(url).done(function (data) {
            content = data;
            if ($('#loader').length) {
                replaceContent(content, src);
            }

        }).fail(function () {
            window.location.replace(url);
        });
    }

    $('#body').fadeOut(fadeLength, function () {
        if (content != null) {
            replaceContent(content, src);
        } else {
            placeLoader();
        }
    });
}

function checkURL(hash) {

    if (isLoading) return;

    if (!hash && window.location.hash)
        hash = window.location.hash.substring(1);
    else if (!hash) hash = '';

    //   if (!hash) hash = '';
    if (hash != lasturl) {
        lasturl = hash;
        if (hash == '')
            $('#body').html(defaultContent);
        else
            loadPage(hash);
    }
}

function replaceState(event) {
    event.preventDefault();
    var src = event.target.parentNode.getAttribute("href") || event.target.getAttribute("href");
    checkURL(src);
}

function loadPage(loc) {
    isLoading = true;
    var content;
    var url = loc;
    if (arguments[1]) {
        $.get(url, { datapage: arguments[1] ? arguments[1] : 'none' }).done(function (data) {
            content = data;
            if ($('#loader').length) {
                replaceContent(content, loc);
            }

        }).fail(function () {
            isLoading = false;
            window.location.replace(url);
        });
    } else {
        $.get(url).done(function (data) {
            content = data;
            if ($('#loader').length) {
                replaceContent(content, loc);
            }

        }).fail(function () {
            window.location.replace(url);
        });
    }

    $('#body').fadeOut(fadeLength, function () {
        if (content != null) {
            replaceContent(content, loc);
        } else {
            placeLoader();
        }
    });
}

function placeLoader() {
    $('body').replaceWith('<div class="full-height"><div class="contener_general center-block" id="loader"><div class="contener_mixte"><div class="ballcolor ball_1">&nbsp;</div></div> \
                            <div class="contener_mixte"><div class="ballcolor ball_2">&nbsp;</div></div><div class="contener_mixte"><div class="ballcolor ball_3">&nbsp;</div></div> \
                            <div class="contener_mixte"><div class="ballcolor ball_4">&nbsp;</div></div></div></div>');
}

function getContent(state) {
    var content;
    var url = state.location ? state.location : '/';
    $.get(url).done(function (data) {
        content = data;
        if ($('#loader').length) {
            replaceContent(content, url);
        }
    }).fail(function () {
        window.location.replace(url);
    });
    $('#body').fadeOut(fadeLength, function () {
        if (content != null) {
            replaceContent(content, state.location, false);
        } else {
            placeLoader();
        }
    });
}

function hashGet(loc) {
    $('#body').css({ opacity: '1' });
    $.get(loc).done(function (data) {
        replaceContent(data, loc, false);
    }).fail(function () {
        window.location.replace(loc);
    });
}

function replaceContent() {
    isLoading = false;
    $('#body').html(arguments[0]);
    console.log('window loc' + window.location.pathname);
    console.log('replace loc' + arguments[1]);
    if (arguments[2] !== false) {
        history.pushState({ location: arguments[1] }, '', '#'.concat(arguments[1]));
    }
    $('#body').fadeIn(fadeLength);
    bindEventHandlers();
}

function bindEventHandlers() {
    //var links = $('a');
    //links.each(function (i) {
    //    if ($(this).attr("data-internal") === 'true') {
    //        $(this).bind('click', replaceState);
    //        // $(this).bind('click', loadContent);
    //    }
    //});
}

$(document).ready(function () {
    //defaultContent = $('#body').html();
    //setInterval('checkURL()', 100);
    //window.onbeforeunload = function () { lasturl = ''; }
    //if (window.location.hash) {
    //    var hash = window.location.hash.substring(1);
    //    hashGet(hash);
    //}
    //window.addEventListener("popstate", function (event) {           
    //    if (event.state || history.state) {
    //        getContent(event.state || history.state);
    //    }
    //});

    if (!window.location.hash) {
        //var locationSource = location.pathname == '/' ? '' : location.pathname;
        //history.replaceState({ location: locationSource }, '', locationSource);
    }
    // bindEventHandlers();
});